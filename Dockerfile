FROM node:15.11.0
WORKDIR '/app'
COPY ./package.json ./
COPY ./code ./
RUN npm install
RUN npm install pm2 -g
CMD pm2-runtime '/app/bin/www'
