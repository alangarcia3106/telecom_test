let express = require('express');
let router = express.Router();

router.post('/get_word', getWord)

async function getWord( req, res ) {
  let { matrix, word } = req.body;
  let result = false;

  try {
    matrix = JSON.parse( matrix );
  } catch (e) {
    res.status(500);
    res.send( { result } );
  }
  for (let i = 0; i < matrix.length; i++) {
    for (let j = 0; j < matrix[i].length; j++) {
      let actualPos = 0;
      let direction = 2;
      result = searchNext(matrix, word, direction, j, i, actualPos, [i, j]);
      if (result) {
        i = matrix.length;
        break;
      }
    }
  }
  res.status(200);
  res.send( { result } );

}


/**
 *
 * @param matrix
 * @param word
 * @param direction
 * @param x
 * @param y
 * @param actualPos
 * @param checkpoint
 * @returns {boolean|*}
 */
function searchNext(matrix, word, direction, x, y, actualPos, checkpoint) {
  if (actualPos >= word.length)
    return true;
  if (  matrix[y] && matrix[y][x] && matrix[y][x] === word[actualPos] ) {
    checkpoint = [y, x];
    switch (direction) {
      case 1: y--; break; // up
      case 2: x++; break; // right
      case 3: y++; break; // down
      case 4: x--; break; // left
      case 5: y--; break; // up
      case 6: x++; break; // right
      case 7: y++; break; // down
      case 8: x--; break; // left
    }
    actualPos++;
    return searchNext(matrix, word, direction, x, y, actualPos, checkpoint);
  } else {
    direction++;
    y = checkpoint[0];
    x = checkpoint[1];

    switch (direction) {
      case 1: y--; break; // up
      case 2: x++; break; // right
      case 3: y++; break; // down
      case 4: x--; break; // left
      case 5: y--; break; // up
      case 6: x++; break; // right
      case 7: y++; break; // down
      case 8: x--; break; // left
      default:
        return false;
    }
    return searchNext(matrix, word, direction, x, y, actualPos, checkpoint);
  }

}

module.exports = router;
