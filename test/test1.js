let chai = require('chai');
let chaiHttp = require('chai-http');
let expect = require('chai').expect;
chai.use(chaiHttp);

const baseUrl = 'http://localhost:3000';

describe( 'test word in board', ()=> {
    it('should get true', function (done) {
        const matrix = [
            [ "a", "a", "c", "d" ],
            [ "e", "e", "c", "v" ],
            [ "g", "a", "c", "b" ]
        ];
        chai.request(baseUrl)
            .post('/get_word')
            .send({ matrix: JSON.stringify(matrix), word: 'ccv' })
            .end( ((err, res) => {
                console.log(res.body)
                expect( res.body.result ).to.equal(true);
                done();
            }) )
    });
})
